# NILOD

***Node.js Interactive Lines Of Dialog***

A lightweight chatbot library for Node.js, inspired by Python 3's COBE 2 chatbot,
which supports JSON importing and exporting, and unsupervised,
peer-assisted machine learning to help select the best output sentences.

The project is under the MIT license; for more information, please see the LICENSE file.

## How to Use

It's reasonably simple to make use of this library. This snippet tersely shows
the many features from the library:

```js
const NILOD = require('nilod')

let model = new NILOD('myDatabase.sqlite');

// for example purposes, I'll define a simple function to help
// print our model's output :)
function printSentence(res) {
    console.log(`(${res.outputWords.length}) ${res.output}`);
}

model.train([
    'Hello, oh beautiful world I have come upon... here, rise flowers, the world spins and rotates beneath my little-dwinkle tiptoesome feet!',
    'Bequeath me thy burden, and I\'ll engrave on thee my sweetness.'
]);

// Let's quickly generate one sentence, without the whole selection process.
let res = model.tryOnce();
printSentence(res);

// Hey, I like that sentence!
// Let's give it a good rating.
res.applyRating(5);

// Ratings will help our model learn to generate better results,
// when you use the fancy 'generate' function, which generates
// a bunch of sentences, and selects the best one using past ratings.

// In this case, let's use 30 samples (candidates to selection).
let best = model.generate(30).best;
printSentence(best.attempt);
```